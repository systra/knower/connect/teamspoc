Other samples to read
=====================

- For interraction between users (and possibly notif): https://github.com/OfficeDev/Microsoft-Teams-Samples/tree/main/samples/tab-request-approval/nodejs
- For navigation between tabs: https://github.com/OfficeDev/Microsoft-Teams-Samples/tree/main/samples/tab-app-navigation/nodejs
- For its UI for login: https://github.com/OfficeDev/Microsoft-Teams-Samples/tree/main/samples/tab-adaptive-cards/nodejs
- For permission handling: https://github.com/OfficeDev/Microsoft-Teams-Samples/tree/main/samples/tab-staggered-permission/nodejs
