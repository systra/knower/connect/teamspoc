// Generate one (using `python -m uuid`) and stick to it
local teamsAppId = std.extVar('TEAMS_APP_ID');
local clientId = std.extVar('TEAMS_CLIENT_ID');
// Comma separated domain list
local domains = std.split(std.stripChars(std.extVar('TEAMS_DOMAINS'), ' ,'), ',');
local mainDomain = domains[0];
local apiDomain = std.extVar('TEAMS_API_DOMAIN');

{
  '$schema': 'https://developer.microsoft.com/json-schemas/teams/v1.16/MicrosoftTeams.schema.json',
  manifestVersion: '1.16',
  version: '1.0.0',
  id: teamsAppId,
  packageName: 'digital.systra.sds.testsso',
  developer: {
    name: 'Microsoft',
    websiteUrl: 'https://www.microsoft.com',
    privacyUrl: 'https://www.microsoft.com/privacy',
    termsOfUseUrl: 'https://www.microsoft.com/termsofuse',
  },
  name: {
    short: 'Teams PoC Cyrille',
    full: 'Teams PoC Cyrille',
  },
  description: {
    short: 'Teams PoC Cyrille',
    full: 'Teams PoC Cyrille',
  },
  icons: {
    outline: 'outline.png',
    color: 'color.png',
  },
  accentColor: '#60A18E',
  staticTabs: [
    {
      entityId: 'auth',
      name: 'Auth',
      contentUrl: 'https://' + mainDomain + '/ssoDemo',
      scopes: [
        'personal',
      ],
    },
    {
      entityId: 'configure',
      name: 'Configure',
      contentUrl: 'https://' + mainDomain + '/configure',
      scopes: [
        'personal',
      ],
    },
  ],
  validDomains: domains,
  webApplicationInfo: {
    id: clientId,
    resource: 'api://' + apiDomain + '/' + clientId,
  },
}
