"""
Django settings for project.
"""
from os import getenv
from pathlib import Path
from typing import overload


@overload
def from_env(key: str, default: bool) -> bool: ...  # noqa: E704
@overload
def from_env(key: str, default: str) -> str: ...  # noqa: E704
@overload
def from_env(key: str, default: None) -> str | None: ...  # noqa: E704
@overload
def from_env(key: str) -> str | None: ...  # noqa: E704


def from_env(key: str, default: str | bool | None = None) -> str | bool | None:
    match default:
        case str():
            return getenv(key, default)
        case bool():
            return getenv(key, str(default)).lower() in ('true', '1', 'on')
        case None:
            return getenv(key)


BASE_DIR = Path(__file__).resolve(strict=True).parent.parent

# Use `./manage.py generate_secret_key` to generate a new one
SECRET_KEY = from_env('DJANGO_SECRET_KEY', 'django-insecure-lynib28u(i_#q*^eue&#1^&j=gnj+-l#&=hpz6l_jzhuzk&gso')
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = from_env('DJANGO_DEBUG', False)
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'root': {  # catch all for all loggers
        'handlers': ['console'],
        'level': 'DEBUG' if DEBUG else 'WARNING',
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': from_env('DJANGO_LOG_LEVEL', 'INFO' if DEBUG else 'WARNING'),
            'propagate': False,
        },
        'flake8': {
            'level': 'ERROR',
        },
        'oauth2_authcodeflow': {
            'level': 'DEBUG',
        },
    },
}
ALLOWED_HOSTS = ['*']
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'oauth2_authcodeflow',
    'server',
]
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'server.middlewares.BrowserRefreshAccessTokenMiddleware',  # for browser
    'server.middlewares.BrowserRefreshSessionMiddleware',  # for browser
    'server.middlewares.TeamsRefreshAccessTokenMiddleware',  # for teams
    'server.middlewares.InjectMsAuthHeaderMiddleware',  # for ms graph
]
ROOT_URLCONF = 'server.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
WSGI_APPLICATION = 'wsgi.application'

# Database
# https://docs.djangoproject.com/en/5.1/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db' / 'db.sqlite3',
    },
}
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

SESSION_COOKIE_AGE = 30 * 86400  # 30 days
X_FRAME_OPTIONS = ''  # Allow to be included in a i-frame (for Teams)
# https://github.com/AzureAD/microsoft-authentication-library-for-js/issues/5709#issuecomment-1446096814
SECURE_CROSS_ORIGIN_OPENER_POLICY = 'unsafe-none'  # (for Browser popup)
# https://github.com/OttoYiu/django-cors-headers#configuration
CORS_ALLOW_ALL_ORIGINS = True
CORS_ALLOW_CREDENTIALS = True
SESSION_COOKIE_SAMESITE = 'None'
SESSION_COOKIE_SECURE = True
# Auth (Teams or Browser)
AZURE_TENANT_ID = from_env('AZURE_TENANT_ID')
OIDC_OP_DISCOVERY_DOCUMENT_URL = f'https://login.microsoftonline.com/{AZURE_TENANT_ID}/v2.0/.well-known/openid-configuration'
OIDC_RP_CLIENT_ID = from_env('OIDC_RP_CLIENT_ID')
OIDC_RP_CLIENT_SECRET = from_env('OIDC_RP_CLIENT_SECRET')
OIDC_RP_AZURE_SPA = True
OIDC_RP_SCOPES = [
    'openid',
    'offline_access',
    'email',
    'profile',
    'User.Read',
    'User.ReadBasic.all',
]
OIDC_MIDDLEWARE_NO_AUTH_URL_PATTERNS = [
    '/isAuthenticated',
    '/ssoDemo',
    '/Home/BrowserRedirect',
    '/auth-fail',
    '/auth-start',
    '/auth-end',
    '/teamsStoreAccessToken',
]
OIDC_MIDDLEWARE_API_URL_PATTERNS = ['/getProfile', '/getUsersInOrg']
TEAMS_DOMAINS = from_env('TEAMS_DOMAINS', 'knowerconnect.loca.lt').split(',')
AZURE_APP_ID_URI = f'api://{TEAMS_DOMAINS[0]}/{OIDC_RP_CLIENT_ID}'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Europe/Paris'
USE_I18N = True
USE_TZ = True
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

STATIC_URL = 'static/'
STATIC_ROOT = BASE_DIR / 'static'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'npm.finders.NpmFinder',
)
NPM_ROOT_PATH = BASE_DIR
NPM_EXECUTABLE_PATH = 'yarn'
NPM_FILE_PATTERNS = {
    '@microsoft/teams-js': ['dist/*.min.js'],
    '@azure/msal-browser': ['lib/*.min.js'],
    'jquery': ['dist/*.min.js'],
}
AUTH_USER_MODEL = 'server.User'
AUTHENTICATION_BACKENDS = [
    'oauth2_authcodeflow.auth.AuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend',
]
