from django.urls import (
    include,
    path,
)

from .views import (
    browser_redirect_page,
    fail_page,
    get_profile,
    get_users_in_organization,
    is_authenticated,
    main_page,
    teams_consent_page,
    teams_consent_post_page,
    teams_store_access_token,
)

urlpatterns = [
    path('oidc/', include('oauth2_authcodeflow.urls')),
    path('ssoDemo', main_page, name='main'),
    path('isAuthenticated', is_authenticated, name='is_authenticated'),
    path('Home/BrowserRedirect', browser_redirect_page, name='browser_redirect'),
    path('auth-fail', fail_page, name='browser_fail'),
    path('auth-start', teams_consent_page, name='teams_consent'),
    path('auth-end', teams_consent_post_page, name='teams_consent_post'),
    path('teamsStoreAccessToken', teams_store_access_token, name='teams_store_access_token'),
    path('getProfile', get_profile, name='get_profile'),
    path('getUsersInOrg', get_users_in_organization,  name='users_in_organization'),
]
