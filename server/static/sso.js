class Sso {
  constructor(isAuthenticatedUrl, authenticateUrl, failUrl, teamsStoreAccessTokenUrl, teamsAuthStartUrl) {
    this.isAuthenticatedUrl = isAuthenticatedUrl;
    this.authenticateUrl = authenticateUrl;
    this.failUrl = location.origin + failUrl;
    this.teamsStoreAccessTokenUrl = teamsStoreAccessTokenUrl;
    this.teamsAuthStartUrl = teamsAuthStartUrl;
    this.inTeams = undefined;
  }
  async _isInTeams() {
    try {
      await window.microsoftTeams.app.initialize();
      return true;
    } catch (error) {
      return false;
    }
  }
  async beforeSsoCallback() {}
  async showTeamsConsent() {}
  async afterLogin() {}
  async apiCall(...params) {
    const response = await fetch(...params)
    if (response.ok) {
      return await response.json()
    } else {
      console.debug(`${response.status} ${response.statusText}`);
      if (response.status == 401) {
        const canContinue = await reauthFunc();
        throw {
          reauth: true,
          canContinue,
        };
      } else {
        throw response.statusText;
      }
    }
  }
  async init() {
    if (this.inTeams !== undefined) {
      return;
    }
    this.inTeams = await this._isInTeams();
    await this.beforeSsoCallback();
    const reauthFunc = this.inTeams ? await this._initTeams() : await this._initBrowser();
    let canContinue = false;
    try {
      const authenticatedResponse = await this.apiCall(this.isAuthenticatedUrl);
      canContinue = authenticatedResponse.authenticated ? true : await reauthFunc();
    } catch (err) {
      canContinue = err.reauth ? err.canContinue : false;
    }
    if (canContinue) {
        await this._ready();
    }
    return canContinue
  }
  async _initTeams() {
    // https://learn.microsoft.com/fr-fr/microsoftteams/platform/tabs/how-to/authentication/auth-tab-aad?tabs=teamsjs-v2
    return await teamsSSO(this.teamsStoreAccessTokenUrl, this.teamsAuthStartUrl, this.showTeamsConsent.bind(this));
  }
  async _initBrowser() {
    return await browserSSO(this.authenticateUrl, this.failUrl);
  }
  async _ready() {
    await this.afterLogin();
    console.debug("********** APP IS READY **********");
  }
}
