async function browserSSO (authenticateUrl, failUrl) {
  return function() {
    const nextUrl = '' + window.location
    const url = `${authenticateUrl}?next=${nextUrl}&fail=${failUrl}`;
    console.log(`redirect to ${url}`);
    window.location = url;
    return false;
  }
}
