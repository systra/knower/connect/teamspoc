class MyApp {
  constructor(sso) {
    sso.beforeSsoCallback = this.beforeSsoCallback.bind(this);
    sso.showTeamsConsent = this.showTeamsConsent.bind(this);
    sso.afterLogin = this.afterLogin.bind(this);
    this.sso = sso;
  }
  beforeSsoCallback() {
    if (this.sso.inTeams) {
      console.log("🎊 In Teams 🎊")
    } else {
      console.log("🪪 Not in Teams, allow to login from Browser 🪪")
    }
  }
  showTeamsConsent(teamsConsentPopup) {
    $('#teams-consent').show();
    $('#teams-consent button').on('click', teamsConsentPopup);
    $('#init').hide();
  }
  afterLogin() {
    if (this.sso.inTeams) {
      console.log("🎉 User signed in from Teams");
    } else {
      console.log("🎉 User signed in from Browser");
      $('#browser-header').show();
    }
    $('#init').hide();
  }
  async init() {
    // hide all except init section
    $('#content').hide();
    $('#browser-header').hide();
    $('#teams-consent').hide();
    $('#init').show();
    if (!await this.sso.init()) {
      return;
    }
    await this.showProfile()
    await this.showOrgUsers()
    $('#content').show();
  }
  async showProfile() {
    try {
      const profile = await this.getProfile();
      this.displayProfile(profile);
    } catch (err) {
      console.error("Cannot get profile", err);
    }
  }
  async getProfile() {
    return await this.sso.apiCall('/getProfile', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
      credentials: 'include',
    });
  }
  displayProfile(profile) {
    const profileDiv = $('#content .profile > div');
    profileDiv.empty();
    profileDiv.html(
      Object.entries(profile).filter(
        ([k, v]) => !k.startsWith('@') && v && (!Array.isArray(v) || v.length),
      ).map(
        ([k, v]) => `${k} = ${v}`,
      ).join('<br>\n'),
    );
  }
  async showOrgUsers() {
    try {
      const orgUsers = await this.getOrgUsers();
      this.displayOrgUsers(orgUsers);
    } catch (err) {
      console.error("Cannot get Org users", err);
    }
  }
  async getOrgUsers() {
    const res = await this.sso.apiCall('/getUsersInOrg', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      cache: 'default',
      credentials: 'include',
    });
    return res.value;
  }
  displayOrgUsers(orgUsers) {
    const ul = $(".users ul");
    ul.empty();
    orgUsers.forEach(user => {
      ul.append("<li>" + user.displayName + "</li>");
    })
  }
}
