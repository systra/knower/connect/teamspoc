async function teamsSSO(teamsStoreAccessTokenUrl, teamsAuthStartUrl, showTeamsConsent) {
  const jsonToken = await getJsonToken();
  return async function() {
    try {
      await storeAccessToken(jsonToken, teamsStoreAccessTokenUrl);
      return true;
    } catch (error) {
      if (error.error === 'invalid_grant' && error.suberror === 'consent_required') {
        console.debug("Error - user or admin consent required", error);
        await showTeamsConsent(async () => await teamsConsent(teamsAuthStartUrl));
        return true;
      } else {
        console.error(error);
        throw error;
        return false;
      }
    }
  }
}

async function getJsonToken() {
  // Get auth token
  // Ask Teams to get us a token from AAD
  try {
    return await window.microsoftTeams.authentication.getAuthToken();
  } catch (error) {
    throw `Error getting token: ${error}`;
  }
}

async function storeAccessToken(jsonToken, teamsStoreAccessTokenUrl) {
  // Exchange that token for a token with the required permissions
  // using the web service (see /auth/token handler in app.js)
  const context = await window.microsoftTeams.app.getContext();
  const response = await fetch(teamsStoreAccessTokenUrl, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      'tid': context.user.tenant.id,
      'token': jsonToken,
    }),
    mode: 'cors',
    cache: 'default',
    credentials: 'include',
  });
  if (!response.ok) {
    throw new Error(`${response.status} ${response.statusText}`);
  }
  const responseJson = await response.json();
  if (responseJson.error) {
    throw responseJson;
  }
}

async function teamsConsent(teamsAuthStartUrl) {
  try {
    // Show the consent pop-up
    // await is waiting for the popup to be closed
    const result = await window.microsoftTeams.authentication.authenticate({
      url: teamsAuthStartUrl,
      width: 600,
      height: 535,
    });
    console.debug('Consent succeeded');
    const data = JSON.parse(result);
    for (const [key, value] of Object.entries(data.sessionStorage)) {
      sessionStorage.setItem(key, value);
    }
    console.debug(data)
    window.location.reload()
  } catch (error) {
    console.error(`ERROR ${JSON.stringify(error)}`);
  }
}

async function initMsal(clientId) {
  const app = window.microsoftTeams.app;
  // Get the tab context, and use the information to navigate to Azure AD login page
  await app.initialize();
  const context = await app.getContext();
  const msalConfig = {
    auth: {
      clientId,
      authority: `https://login.microsoftonline.com/${context.user.tenant.id}`,
      navigateToLoginRequestUrl: false,
    },
    cache: {
      cacheLocation: "sessionStorage",
    },
  };
  const msalInstance = new window.msal.PublicClientApplication(msalConfig);
  await msalInstance.initialize();
  return msalInstance;
}

async function teamsConsentStart(clientId, teamsAuthEndUrl) {
  const msalInstance = await initMsal(clientId);
  const scopes = [
    "email",
    "openid",
    "profile",
    "offline_access",
    "User.Read",
    "User.ReadBasic.all",
  ];
  await msalInstance.loginRedirect({
    scopes,
    redirectUri: teamsAuthEndUrl,
    loginHint: context.user.loginHint,
  });
}
async function teamsConsentEnd(clientId) {
  const msalInstance = await initMsal(clientId);
  // notifySuccess or notifyFailure should be called
  // https://learn.microsoft.com/fr-fr/microsoftteams/platform/tabs/how-to/authentication/auth-tab-aad?tabs=teamsjs-v2#the-callback-page
  const auth = window.microsoftTeams.authentication;
  try {
    const tokenResponse = await msalInstance.handleRedirectPromise();
    if (tokenResponse !== null) {
      auth.notifySuccess(JSON.stringify({ sessionStorage }));
    } else {
      auth.notifyFailure("Get empty response.");
    }
  } catch (error) {
    auth.notifyFailure(JSON.stringify(error));
  }
}
