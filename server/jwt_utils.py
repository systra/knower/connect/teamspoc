import base64
import json


def decode_token(full_token):
    encoded_token = full_token.split('.')[1]
    encoded_token += '=' * ((4 - len(encoded_token) % 4) % 4)
    decoded_token = str(base64.b64decode(encoded_token), 'utf-8')
    return json.loads(decoded_token)
