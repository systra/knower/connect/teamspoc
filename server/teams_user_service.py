from datetime import datetime
from json import loads
from zoneinfo import ZoneInfo

from oauth2_authcodeflow.conf import settings
from oauth2_authcodeflow.constants import (
    SESSION_ACCESS_TOKEN,
    SESSION_REFRESH_TOKEN,
)
from requests import request as http_request

from server.jwt_utils import decode_token
from server.models import User

SESSION_TOKEN_TYPE = 'oidc_token_type'
SESSION_WITH_TEAMS = 'teams'


def obtain_new_token(request) -> dict[str, str]:
    json_request = loads(request.read())
    jwt_token = json_request['token']
    conf = http_request(method='GET', url=settings.OIDC_OP_DISCOVERY_DOCUMENT_URL).json()
    token_endpoint = conf['token_endpoint']
    response = http_request(
        method='POST',
        url=token_endpoint,
        headers=dict(
            accept='application/json',
        ),
        data=dict(
            # https://datatracker.ietf.org/doc/html/rfc7523
            grant_type='urn:ietf:params:oauth:grant-type:jwt-bearer',
            requested_token_use='on_behalf_of',
            client_id=settings.OIDC_RP_CLIENT_ID,
            client_secret=settings.OIDC_RP_CLIENT_SECRET,
            scope=' '.join(settings.OIDC_RP_SCOPES),
            assertion=jwt_token,
        ),
    )
    response.raise_for_status()
    token_info = response.json()
    if 'error' in token_info:
        # if a consent is required, it should return
        # dict(error='invalid_grant', suberror='consent_required')
        return token_info
    else:
        # return dict(error='invalid_grant', suberror='consent_required')
        _get_or_create_teams_user(token_info['id_token'])
        request.session[SESSION_ACCESS_TOKEN] = token_info['access_token']
        request.session[SESSION_REFRESH_TOKEN] = token_info['refresh_token']
        request.session[SESSION_TOKEN_TYPE] = token_info['token_type']
        request.session[SESSION_WITH_TEAMS] = True
        request.session.save()
        return {}


def _get_or_create_teams_user(id_token):
    email, last_name, first_name, username = _get_user_data(id_token)
    try:
        return User.objects.get(username=username)
    except User.DoesNotExist:
        return User.objects.create(
            email=email,
            last_name=last_name,
            first_name=first_name,
            username=username,
        )


def _get_user_data(id_token):
    token_data = decode_token(id_token)
    return (
        token_data[settings.OIDC_OP_EXPECTED_EMAIL_CLAIM],
        token_data.get(settings.OIDC_LASTNAME_CLAIM, ''),
        token_data.get(settings.OIDC_FIRSTNAME_CLAIM, ''),
        token_data['oid'],
    )


def is_in_teams(request) -> bool:
    return request.session.get(SESSION_WITH_TEAMS, False)


def has_access_token(request) -> bool:
    return SESSION_ACCESS_TOKEN in request.session


def is_access_token_expiring_soon(request) -> bool:
    if access_token := request.session.get(SESSION_ACCESS_TOKEN):
        return _is_token_expired(access_token)
    else:
        refresh_token = request.session.get(SESSION_REFRESH_TOKEN)
        return bool(refresh_token)  # only True if refresh_token exists


def refresh_access_token(request) -> None:
    conf = http_request(method='GET', url=settings.OIDC_OP_DISCOVERY_DOCUMENT_URL).json()
    token_endpoint = conf['token_endpoint']
    refresh_token = request.session.get(SESSION_REFRESH_TOKEN)
    if _is_token_expired(refresh_token, 10):
        request.session.pop(SESSION_ACCESS_TOKEN, None)
        request.session.pop(SESSION_REFRESH_TOKEN, None)
        request.session.pop(SESSION_TOKEN_TYPE, None)
        request.session.pop(SESSION_WITH_TEAMS, None)
    else:
        result = http_request(
            method='POST',
            url=token_endpoint,
            data=dict(
                grant_type='refresh_token',
                client_id=settings.OIDC_RP_CLIENT_ID,
                client_secret=settings.OIDC_RP_CLIENT_SECRET,
                refresh_token=refresh_token,
            ),
        ).json()
        request.session[SESSION_ACCESS_TOKEN] = result['access_token']
        if (refresh_token := result.get('refresh_token')):
            request.session[SESSION_REFRESH_TOKEN] = refresh_token
            request.session[SESSION_TOKEN_TYPE] = result['token_type']
        request.session[SESSION_WITH_TEAMS] = True
    request.session.save()


def _is_token_expired(token: str, threshold_in_second: int = 600) -> bool:
    token_data = decode_token(token)
    exp_timestamp = token_data['exp']
    now = datetime.now(tz=ZoneInfo('UTC')).timestamp()
    before_end = exp_timestamp - threshold_in_second
    return now >= before_end
