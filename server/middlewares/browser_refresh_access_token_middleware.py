from django.http.request import HttpRequest
from django.http.response import HttpResponse
from oauth2_authcodeflow.middleware import RefreshAccessTokenMiddleware

from server.teams_user_service import is_in_teams


class BrowserRefreshAccessTokenMiddleware(RefreshAccessTokenMiddleware):
    def process_request(self, request: HttpRequest) -> HttpResponse | None:
        if not is_in_teams(request):
            return super().process_request(request)
