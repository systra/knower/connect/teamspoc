from oauth2_authcodeflow.constants import SESSION_ACCESS_TOKEN

from ..teams_user_service import SESSION_TOKEN_TYPE


class InjectMsAuthHeaderMiddleware:
    MS_AUTH_HEADER = 'HTTP_X_MS_AUTHORIZATION'

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(self.process_request(request))

    def process_request(self, request):
        if (access_token := request.session.get(SESSION_ACCESS_TOKEN)):
            token_type = request.session.get(SESSION_TOKEN_TYPE, 'Bearer')
            request.META[self.MS_AUTH_HEADER] = f"{token_type} {access_token}"
        return request

    @classmethod
    def get_headers(cls, request) -> dict[str, str]:
        return dict(
            accept='application/json',
            authorization=request.headers.get(cls.MS_AUTH_HEADER.replace('HTTP_', '').replace('_', '-').lower()),
        )
