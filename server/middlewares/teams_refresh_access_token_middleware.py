from server.teams_user_service import (
    is_access_token_expiring_soon,
    is_in_teams,
    refresh_access_token,
)


class TeamsRefreshAccessTokenMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(self.process_request(request))

    def process_request(self, request):
        if is_in_teams(request) and is_access_token_expiring_soon(request):
            refresh_access_token(request)
        return request
