from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import (
    redirect,
    render,
)
from django.urls import reverse
from django.views.decorators.http import (
    require_GET,
    require_POST,
)
from oauth2_authcodeflow.constants import OIDC_URL_AUTHENTICATION_NAME
from requests import request as http_request

from server.middlewares import InjectMsAuthHeaderMiddleware
from server.teams_user_service import (
    has_access_token,
    obtain_new_token,
)


@require_GET
def main_page(request):
    return render(request, 'main.html', {
        'is_authenticated_url': request.build_absolute_uri(reverse('is_authenticated')),
        'authenticate_url': request.build_absolute_uri(reverse(OIDC_URL_AUTHENTICATION_NAME)),
        'fail_url': reverse('browser_fail'),  # complete with location.origin in front
        'teams_store_access_token_url': request.build_absolute_uri(reverse('teams_store_access_token')),
        'teams_auth_start_url': request.build_absolute_uri(reverse('teams_consent')),
    })


@require_GET
def is_authenticated(request) -> JsonResponse:
    return JsonResponse(dict(authenticated=request.user.is_authenticated))


@require_GET
def browser_redirect_page(request):
    return redirect(reverse('main'))


@require_GET
def fail_page(request):
    return render(request, 'fail.html', {
        'error': request.GET.get('error'),
    })


@require_POST
def teams_store_access_token(request) -> JsonResponse:
    return JsonResponse({} if has_access_token(request) else obtain_new_token(request))


@require_GET
def teams_consent_page(request):
    return render(request, 'teams-consent.html', {
        'clientId': settings.OIDC_RP_CLIENT_ID,
        'teams_auth_end_url': reverse('teams_consent_post'),
    })


@require_GET
def teams_consent_post_page(request):
    return render(request, 'teams-consent-post.html', {
        'clientId': settings.OIDC_RP_CLIENT_ID,
    })


@require_POST
def get_profile(request):
    response = http_request(
        method='GET',
        url='https://graph.microsoft.com/v1.0/me/',
        headers=InjectMsAuthHeaderMiddleware.get_headers(request),
    )
    response.raise_for_status()
    return JsonResponse(response.json())


@require_POST
def get_users_in_organization(request):
    response = http_request(
        method='GET',
        url='https://graph.microsoft.com/v1.0/users/?$top=10',
        headers=InjectMsAuthHeaderMiddleware.get_headers(request),
    )
    response.raise_for_status()
    return JsonResponse(response.json())
