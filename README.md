How to run locally
------------------

Requirements:
- A network that allows arbitrary tcp output connexion OR a remote Linux server with SSH access
- App configured on Azure Portal
- Microsoft Account that can side-load applications on Teams
- Node/Yarn/Python
- Browser
- Patience

Steps:
- Create an App on Azure.
- Fill the `.env` file from `env.dist` file then source the `.env` file: `source .env`
- `yarn install`
- `yarn run generateAzureManifest` to get a manifest to apply to Azure App. You may to apply secrets related settings.
- for bad-citizen network: `yarn run sshuttle`
- `yarn run proxy`
- `yarn run start`
- `yarn run teamsZip`
- In your browser, open a containered window or a new profile on https://teams.microsoft.com/v2/. Connect with the dev Tenant.
- Install the `DevPortal` app.
- In `DevPortal`, in `Apps`, click `Import` and select the `knowerconnectPoC.zip` file.
- When in app details, click `Preview`, then `Add`
