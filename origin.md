Origin
======

The following code have been created using [Microsoft Teams Samples repo](https://github.com/OfficeDev/Microsoft-Teams-Samples.git).

The exact example resides in the `samples/tab-sso/nodejs` folder.

The `msteams-16.css` could be from [this npm project](https://www.npmjs.com/package/msteams-ui-styles-core).
