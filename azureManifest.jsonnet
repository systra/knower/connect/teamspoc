// Given by Azure
local azureId = std.extVar('AZURE_APP_ID');
local clientId = std.extVar('TEAMS_CLIENT_ID');
// Generate by Azure (step 10)
local permissionId = std.extVar('TEAMS_PERM_ID');
// Comma separated domain list
local domains = std.split(std.stripChars(std.extVar('TEAMS_DOMAINS'), ' ,'), ',');
local mainDomain = domains[0];
local microsoftTeamsDesktopClientId = '1fec8e78-bce4-4aaf-ab1b-5451cc387264';
local microsoftTeamsWebClientId = '5e3ce6c0-2b1f-4285-8d4b-75ee78787346';
local redirectUris = [
  'http://localhost:3978/Home/BrowserRedirect',
  'http://localhost:3978/auth-end',
] + [
  'https://' + domain + '/Home/BrowserRedirect'
  for domain in domains
] + [
  'https://' + domain + '/auth-end'
  for domain in domains
];
local graphPermId = '00000003-0000-0000-c000-000000000000';
local graphEmailPermId = '64a6cdd6-aab1-4aaf-94b8-3cc8405e90d0';
local graphOfflineAccessPermId = '7427e0e9-2fba-42fe-b0c0-848c9e6a8182';
local graphOpenidPermId = '37f7f235-527c-4136-accd-4a02d197296e';
local graphProfilePermId = '14dad69e-099b-42c9-810b-d002981feec1';
local graphUserReadPermId = 'e1fe6dd8-ba31-4d61-89e7-88639da4683d';

{
  id: azureId,
  deletedDateTime: null,
  appId: clientId,
  applicationTemplateId: null,
  disabledByMicrosoftStatus: null,
  createdDateTime: '2024-04-29T14:57:20Z',
  displayName: 'KnowerConnect',
  description: null,
  groupMembershipClaims: 'None',
  identifierUris: [
    'api://' + mainDomain + '/' + clientId,
  ],
  isDeviceOnlyAuthSupported: null,
  isFallbackPublicClient: null,
  nativeAuthenticationApisEnabled: null,
  notes: null,
  publisherDomain: 'BETABETABETABETA.onmicrosoft.com',
  serviceManagementReference: null,
  signInAudience: 'AzureADMultipleOrgs',
  tags: [
    'webApi',
    'apiConsumer',
  ],
  tokenEncryptionKeyId: null,
  samlMetadataUrl: null,
  defaultRedirectUri: null,
  certification: null,
  requestSignatureVerification: null,
  addIns: [],
  api: {
    acceptMappedClaims: null,
    knownClientApplications: [],
    requestedAccessTokenVersion: 2,
    oauth2PermissionScopes: [
      {
        adminConsentDescription: 'MSTeams can access user profile via web API',
        adminConsentDisplayName: 'MSTeams can access user profile',
        id: permissionId,
        isEnabled: true,
        type: 'User',
        userConsentDescription: 'MSTeams can access user profile via web API',
        userConsentDisplayName: 'MSTeams can access user profile for KnowerConnect',
        value: 'access_as_user',
      },
    ],
    preAuthorizedApplications: [
      {
        appId: microsoftTeamsDesktopClientId,
        delegatedPermissionIds: [
          permissionId,
        ],
      },
      {
        appId: microsoftTeamsWebClientId,
        delegatedPermissionIds: [
          permissionId,
        ],
      },
    ],
  },
  appRoles: [],
  info: {
    logoUrl: null,
    marketingUrl: null,
    privacyStatementUrl: null,
    supportUrl: null,
    termsOfServiceUrl: null,
  },
  keyCredentials: [],
  optionalClaims: {
    accessToken: [
      {
        additionalProperties: [],
        essential: false,
        name: 'sid',
        source: null,
      },
      {
        additionalProperties: [],
        essential: false,
        name: 'email',
        source: null,
      },
      {
        additionalProperties: [
          'use_guid',
        ],
        essential: false,
        name: 'aud',
        source: null,
      },
    ],
    idToken: [
      {
        additionalProperties: [],
        essential: false,
        name: 'sid',
        source: null,
      },
      {
        additionalProperties: [],
        essential: false,
        name: 'xms_pl',
        source: null,
      },
      {
        additionalProperties: [],
        essential: false,
        name: 'email',
        source: null,
      },
      {
        additionalProperties: [
          'include_externally_authenticated_upn_without_hash',
        ],
        essential: false,
        name: 'upn',
        source: null,
      },
      {
        additionalProperties: [],
        essential: false,
        name: 'family_name',
        source: null,
      },
      {
        additionalProperties: [],
        essential: false,
        name: 'given_name',
        source: null,
      },
      {
        additionalProperties: [],
        essential: false,
        name: 'preferred_username',
        source: null,
      },
    ],
    saml2Token: [],
  },
  parentalControlSettings: {
    countriesBlockedForMinors: [],
    legalAgeGroupRule: 'Allow',
  },
  publicClient: {
    redirectUris: [],
  },
  requiredResourceAccess: [
    {
      resourceAppId: graphPermId,
      resourceAccess: [
        {
          id: graphEmailPermId,
          type: 'Scope',
        },
        {
          id: graphOfflineAccessPermId,
          type: 'Scope',
        },
        {
          id: graphOpenidPermId,
          type: 'Scope',
        },
        {
          id: graphProfilePermId,
          type: 'Scope',
        },
        {
          id: graphUserReadPermId,
          type: 'Scope',
        },
      ],
    },
  ],
  verifiedPublisher: {
    displayName: null,
    verifiedPublisherId: null,
    addedDateTime: null,
  },
  web: {
    homePageUrl: null,
    logoutUrl: null,
    redirectUris: [],
    implicitGrantSettings: {
      enableAccessTokenIssuance: false,
      enableIdTokenIssuance: false,
    },
    redirectUriSettings: [],
  },
  servicePrincipalLockConfiguration: {
    isEnabled: true,
    allProperties: true,
    credentialsWithUsageVerify: true,
    credentialsWithUsageSign: true,
    identifierUris: false,
    tokenEncryptionKeyId: true,
  },
  spa: {
    redirectUris: redirectUris,
  },
}
